/********************************************************************************
** Form generated from reading UI file 'QtOpenGL_001.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QTOPENGL_001_H
#define UI_QTOPENGL_001_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QtOpenGL_001Class
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QWidget *widget;
    QGridLayout *gridLayout;
    QSplitter *splitter_2;
    QWidget *widget_2;
    QSplitter *splitter;
    QWidget *widget_3;
    QGridLayout *gridLayout_3;
    QPushButton *btn_color;
    QWidget *widget_4;
    QWidget *widget_5;
    QWidget *widget_6;
    QGridLayout *gridLayout_4;
    QTabWidget *tabWidget;
    QWidget *Property;
    QWidget *tab_2;
    QMenuBar *menuBar;
    QMenu *menu;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *QtOpenGL_001Class)
    {
        if (QtOpenGL_001Class->objectName().isEmpty())
            QtOpenGL_001Class->setObjectName(QString::fromUtf8("QtOpenGL_001Class"));
        QtOpenGL_001Class->resize(1600, 871);
        centralWidget = new QWidget(QtOpenGL_001Class);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(0);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        widget = new QWidget(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setStyleSheet(QString::fromUtf8("background-color: rgb(83, 83, 83);"));
        gridLayout = new QGridLayout(widget);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        splitter_2 = new QSplitter(widget);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Horizontal);
        widget_2 = new QWidget(splitter_2);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
        widget_2->setSizePolicy(sizePolicy);
        widget_2->setMinimumSize(QSize(50, 0));
        widget_2->setMaximumSize(QSize(16777215, 16777215));
        widget_2->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 170, 255);"));
        splitter_2->addWidget(widget_2);
        splitter = new QSplitter(splitter_2);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        widget_3 = new QWidget(splitter);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(widget_3->sizePolicy().hasHeightForWidth());
        widget_3->setSizePolicy(sizePolicy1);
        widget_3->setMinimumSize(QSize(0, 50));
        widget_3->setMaximumSize(QSize(16777215, 16777215));
        widget_3->setStyleSheet(QString::fromUtf8("background-color: rgb(85, 255, 255);"));
        gridLayout_3 = new QGridLayout(widget_3);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        btn_color = new QPushButton(widget_3);
        btn_color->setObjectName(QString::fromUtf8("btn_color"));

        gridLayout_3->addWidget(btn_color, 0, 0, 1, 1);

        splitter->addWidget(widget_3);
        widget_4 = new QWidget(splitter);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        widget_4->setMinimumSize(QSize(861, 451));
        widget_4->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        splitter->addWidget(widget_4);
        widget_5 = new QWidget(splitter);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));
        sizePolicy1.setHeightForWidth(widget_5->sizePolicy().hasHeightForWidth());
        widget_5->setSizePolicy(sizePolicy1);
        widget_5->setMinimumSize(QSize(0, 50));
        widget_5->setMaximumSize(QSize(16777215, 16777215));
        widget_5->setStyleSheet(QString::fromUtf8("background-color: rgb(85, 255, 255);"));
        splitter->addWidget(widget_5);
        splitter_2->addWidget(splitter);
        widget_6 = new QWidget(splitter_2);
        widget_6->setObjectName(QString::fromUtf8("widget_6"));
        sizePolicy.setHeightForWidth(widget_6->sizePolicy().hasHeightForWidth());
        widget_6->setSizePolicy(sizePolicy);
        widget_6->setMinimumSize(QSize(50, 0));
        widget_6->setMaximumSize(QSize(16777215, 16777215));
        widget_6->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 170, 255);"));
        gridLayout_4 = new QGridLayout(widget_6);
        gridLayout_4->setSpacing(0);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(widget_6);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setStyleSheet(QString::fromUtf8("\n"
"QTabBar::tab:first{\n"
"	font: 12pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"	border: 1px  solid ;\n"
"	border-top-left-radius:8px;\n"
"	border-top-right-radius:8px;\n"
"	border-color: #ecf1f2;\n"
"	width: 100px;\n"
" 	height: 35px;\n"
"	margin-left:2px;\n"
"	color: rgb(102, 102, 102);\n"
"\n"
"	background-color: rgb(236, 241, 242);\n"
"\n"
"\n"
"}\n"
"QTabBar::tab:last{\n"
"	font: 12pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"	border: 1px  solid ;\n"
"	border-top-left-radius:8px;\n"
"	border-top-right-radius:8px;\n"
"	border-color: rgb(236, 241, 242);\n"
"	width: 100px;\n"
" 	height: 35px;\n"
"	color: rgb(102, 102, 102);\n"
"	background-color: rgb(236, 241, 242);\n"
"	\n"
"}\n"
"QTabBar::tab:selected {\n"
"	font: 12pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"	font-weight:bold;\n"
"	background: #5e98ea;\n"
"	color: white;\n"
"	border-top-left-radius:8px;\n"
"	border-top-right-radius:8px;\n"
"	width: 100px;\n"
" 	height: 35px;\n"
"	background-color: #5e98e"
                        "a;\n"
"	border-color: #5e98ea;\n"
"	\n"
"	\n"
"}\n"
"\n"
"QTabWidget::pane{\n"
"\n"
"	margin-top:-10px;\n"
"	margin-left:-10px;\n"
"	margin-right:-10px;\n"
"	margin-bottom:-10px;\n"
"\n"
"}\342\200\213\n"
""));
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabShape(QTabWidget::Triangular);
        Property = new QWidget();
        Property->setObjectName(QString::fromUtf8("Property"));
        tabWidget->addTab(Property, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        tabWidget->addTab(tab_2, QString());

        gridLayout_4->addWidget(tabWidget, 0, 0, 1, 1);

        splitter_2->addWidget(widget_6);

        gridLayout->addWidget(splitter_2, 0, 0, 1, 1);

        widget_2->raise();
        widget_5->raise();
        widget_6->raise();
        widget_3->raise();
        widget_4->raise();
        splitter->raise();
        splitter_2->raise();

        gridLayout_2->addWidget(widget, 0, 0, 1, 1);

        QtOpenGL_001Class->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(QtOpenGL_001Class);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1600, 26));
        menu = new QMenu(menuBar);
        menu->setObjectName(QString::fromUtf8("menu"));
        QtOpenGL_001Class->setMenuBar(menuBar);
        mainToolBar = new QToolBar(QtOpenGL_001Class);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        QtOpenGL_001Class->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(QtOpenGL_001Class);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        QtOpenGL_001Class->setStatusBar(statusBar);

        menuBar->addAction(menu->menuAction());

        retranslateUi(QtOpenGL_001Class);

        QMetaObject::connectSlotsByName(QtOpenGL_001Class);
    } // setupUi

    void retranslateUi(QMainWindow *QtOpenGL_001Class)
    {
        QtOpenGL_001Class->setWindowTitle(QCoreApplication::translate("QtOpenGL_001Class", "QtOpenGL_001", nullptr));
        btn_color->setText(QCoreApplication::translate("QtOpenGL_001Class", "\351\242\234\350\211\262\345\257\271\350\257\235\346\241\206", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Property), QCoreApplication::translate("QtOpenGL_001Class", "Tab 1", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("QtOpenGL_001Class", "\351\241\265\351\235\242\344\272\214", nullptr));
        menu->setTitle(QCoreApplication::translate("QtOpenGL_001Class", "\346\226\207\344\273\266", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QtOpenGL_001Class: public Ui_QtOpenGL_001Class {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QTOPENGL_001_H
