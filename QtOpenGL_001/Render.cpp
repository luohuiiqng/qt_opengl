﻿#include "Render.h"
#include "Common.h"


Render::Render(QWidget* parent)
	: QOpenGLWidget(parent)
{	
}

Render::~Render()
{
	//glDeleteVertexArrays(1, &VAO);
	//glDeleteBuffers(1, &VBO);
	//glDeleteProgram(shaderProgram);
}

void Render::initializeGL()
{
	initializeOpenGLFunctions();//初始化

	QVector<float> p_ve;
	p_ve.append(-0.5f);
	p_ve.append(-0.5f);
	p_ve.append(0.5f);
	p_ve.append(-0.5f);
	p_ve.append(0.0f);
	p_ve.append(0.5f);	
	m_trangle.initDraw(p_ve);
	m_trangle.setName(QStringLiteral("第一个三角形"));
	p_ve.append(-1.0f);
	p_ve.append(0.2f);
	p_ve.append(0.9f);
	p_ve.append(-0.2f);
	p_ve.append(0.0f);
	p_ve.append(1.0f);
	m_trangle_2.initDraw(p_ve);	
	p_ve.clear();
	m_trangle_2.setName(QStringLiteral("第二个三角形"));
	saveObj(&m_trangle);
	saveObj(&m_trangle_2);
}

void Render::resizeGL(int width, int height)
{

}

void Render::paintGL()
{
	//着色
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	for (int i = 0; i < Obj_list.size(); i++) {
		Obj_list.at(i)->Draw();
	}
	update();
}

void Render::saveObj(Trangle *bs) {
	Obj_list.push_back(bs);
	commonData data;
	data.VAO = bs->getVAO();
	data.VBO = bs->getVBO();
	data.name = bs->getName();
	data.str_Ver = bs->getVer();
	data.str_Fragment = bs->getFragment();
	data.vertex = bs->getVertex();
	bs->setCommonData(data);
	sigAddObject(bs->getUid(),bs->getName());
}

Trangle* Render::findObjByUid(QString uid) {
	for (int i = 0; i < Obj_list.size(); i++) {
		if (Obj_list.at(i)->getUid() == uid) {
			return Obj_list.at(i);
		}
	}
	return NULL;
}

