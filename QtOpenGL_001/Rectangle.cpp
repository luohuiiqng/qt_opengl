#include "Rectangle.h"
#include <QDebug>


const char* vertexShaderSource1 = "#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"
"void main()\n"
"{\n"
" gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
"}\0";

const char* fragmentShaderSource1 = "#version 330 core\n"
"out vec4 FragColor;\n"
"uniform vec4 ourColor;\n"
"void main()\n"
"{\n"
" FragColor = ourColor;\n "
"}\0";



void Rectangle::initDraw(QVector<float> point) {
	initializeOpenGLFunctions();//初始化
	isEnable = false;
	if (point.size() < 3) {
		qInfo() << QStringLiteral("最小个数为三.");
		isEnable = false;
		return;
	}
	for (int i = 0; i < point.size(); i++) {
		switch (i)
		{
		case 0:
			vertices[0] = point[i];
		case 1:
			vertices[1] = point[i];
		case 2:
			vertices[3] = point[i];
		case 3:
			vertices[4] = point[i];
		case 4:
			vertices[6] = point[i];
		case 5:
			vertices[7] = point[i];
		default:
			break;
		}
	}
	setVertexShader(vertexShaderSource1);
	setFragmentShader(fragmentShaderSource1);
	setShaderProgram();
	//配置顶点属性	
	glGenVertexArrays(1, &getVAO());
	glGenBuffers(1, &getVBO());

	//绑定VAO
	glBindVertexArray(getVAO());

	//绑定并设置VBO

	glBindBuffer(GL_ARRAY_BUFFER, getVBO());
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	//配置顶点属性
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	isEnable = true;
}


void Rectangle::Draw() {
	//着色	


	glUseProgram(getShaderProgeam());
	int colorLocation = glGetUniformLocation(getShaderProgeam(), "ourColor");
	glUniform4f(colorLocation, m_color.r, m_color.g, m_color.b, m_color.a);
	unsigned int aa = getVAO();
	glBindVertexArray(getVAO());
	glDrawArrays(GL_TRIANGLES, 0, 3);
}
void Rectangle::setColor(COLOR_CUSTOM_RGBA color) {
	m_color = color;
}
void Rectangle::scale(enum class C_Direction direction) {}
void Rectangle::move(enum class C_Direction direction) {}
void Rectangle::rotate(enum class C_Direction direction) {}

