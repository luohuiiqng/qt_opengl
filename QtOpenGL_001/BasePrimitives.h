﻿#pragma once
#include <QString>
#include <QVector>
#include <QOpenGLFunctions_3_3_Core>
#include <QUuid>
#include <QDebug>
#include <QColor>
#include <QMatrix4x4>
#include "Common.h"

class BasePrimitives:protected QOpenGLFunctions_3_3_Core
{
public:
	BasePrimitives();
	~BasePrimitives() {}	
	struct COLOR_CUSTOM_RGBA {
		GLfloat r = -0.1;
		GLfloat g = -0.1;
		GLfloat b = -0.1;
		GLfloat a = -0.1;
	};
	enum class C_Direction
	{
		LEFT,
		RIGHT,
		TOP,
		BOTTOM,
		_X,//正
		_Y,
		_Z,
		X_,//负
		Y_,
		Z_,
	};	 

public:
	glm::mat4 m_transform;
public:

	virtual void initDraw() {}
	virtual void Draw() {}
	virtual void destory() {}
	virtual void scale(MyCustomVector vector) {}
	virtual void move(MyCustomVector vector) {}
	virtual void rotate(MyCustomVector vector) {}
	virtual void setColor(QColor color) {}
	virtual void resetAll(QString uid);

	void setCommonData(commonData data) {
		m_structData.VAO = data.VAO;
		m_structData.VBO = data.VBO;
		m_structData.EBO = data.EBO;
		m_structData.str_Ver = data.str_Ver;
		m_structData.str_Fragment = data.str_Fragment;
		m_structData.vertex = data.vertex;
		m_structData.name = data.name;
	}	
	

	void setName(QString name) {
		m_name = name;
	}
	unsigned int& getVAO() {
		return m_structData.VAO;
	}

	GLuint getVAO_const() {
		return m_structData.VAO;
	}
	unsigned int& getVBO() {
		return m_structData.VBO;
	}
	unsigned int& getEBO() {
		return m_structData.EBO;

	}
	QString& getFragment() {
		return m_structData.str_Fragment;
	}
	QString& getVer() {
		return m_structData.str_Ver;
	}

	QVector<float> getVertex() {
		return m_structData.vertex;
	}

	unsigned int& getVertexShader() {
		return m_structData.vertexShader;
	}

	unsigned int& getFragmentShader() {
		return m_structData.fragmentShader;
	}

	unsigned int& getShaderProgeam() {
		return m_structData.shaderProgram;
	}

	void setShaderProgram();

	void setVertexShader(const char* vertexShaderSource);

	void setFragmentShader(const char* fragmentShaderSource);

	void setVAO(unsigned int vao) {
		m_structData.VAO = vao;
	}
	void setVBO(unsigned int vbo) {
		m_structData.VBO = vbo;
	}
	void setEBO(unsigned int ebo) {
		m_structData.EBO = ebo;

	}
	void setFragment(QString fragment) {
		m_structData.str_Fragment = fragment;
	}
	void setVer(QString ver) {
		m_structData.str_Ver = ver;
	}

	void setVertex(QVector<float> vertex) {
		m_structData.vertex = vertex;
	}

	QString getUid() {
		return m_uid;
	}
	QString getName() {
		return m_name;
	}
private:
	commonData m_structData;
	QString m_uid="";//每个绘制对象的唯一标识符
	QString m_name = "";//对象别名
};

