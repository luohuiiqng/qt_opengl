#pragma once
#include <qwidget.h>
#include <QPushButton>
#include <QHBoxLayout>
#include <QDebug>
class WordWidgetCustom :
    public QWidget
{
    Q_OBJECT
public:
    WordWidgetCustom(QString name, QString uid,int width,int height, QWidget* parent = nullptr):QWidget(parent) {
        m_name = name;
        m_uid = uid;
        this->setLayout(&m_layout);
        m_layout.addWidget(&m_button);
        m_button.setStyleSheet(QString("QPushButton{color:red;background:#111f2c;text-align:left;margin-left:15px;padding-left:5;}"));  
        m_button.setText(name);
        m_name = name;
        m_uid = uid;
        this->setMinimumHeight(height);
        this->setMaximumWidth(width);
        connect(&m_button, &QPushButton::clicked, this, [=]() {
            qDebug() << "EWEWE";
            emit sigClicked(m_uid);
            });
    }
    ~WordWidgetCustom() {};
public:
    signals:
    void sigClicked(QString uid);
private:
    QString m_name;
    QString m_uid;
    QPushButton m_button;
    QHBoxLayout m_layout;
public:
    QString getName() {
        return m_name;
    }
    QString getUid() {
        return m_uid;
    }
};

