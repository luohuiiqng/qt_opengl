﻿#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QDebug>
#include "Trangle.h"

class Render : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
	Q_OBJECT

public:	

	Render(QWidget* parent = 0);
	~Render();
	QVector<Trangle *> Obj_list;
public:
	void saveObj(Trangle *bs);
	Trangle* findObjByUid(QString uid);
public :
	signals:
	void sigAddObject(QString name, QString uid);
protected:
	void initializeGL() override;
	void paintGL() override;
	void resizeGL(int width, int height) override;	
private:
	Trangle m_trangle;
	Trangle m_trangle_2;
	QColor m_color;
};
