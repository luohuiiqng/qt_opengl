﻿#pragma once

#include <QtWidgets/QMainWindow>
#include <QGridLayout>
#include "ui_QtOpenGL_001.h"
#include "Render.h"
#include <QkeyEvent>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QSplitter>
#include "WordWidgetCustom.h"
#include "PropertyControl.h"

class QtOpenGL_001 : public QMainWindow
{
    Q_OBJECT

public:
    QtOpenGL_001(QWidget *parent = nullptr);
    ~QtOpenGL_001();
private:
    void initFace();
    void initTreeOne();
    void initSignal();
public slots:
    void slotColorDialog();
    void slotAddSignal(QString uid, QString name);

    void slotPosition(MyCustomVector position, QString uid);
    void slotRotation(MyCustomVector rotation, QString uid);
    void slotScale(MyCustomVector scale, QString uid);
    void slotObjectChanged(QString uid);
    void slotRsetAll(QString uid);
protected:
    virtual void keyPressEvent(QKeyEvent* ev);
    virtual void keyReleaseEvent(QKeyEvent* ev);
private:
    Ui::QtOpenGL_001Class ui;
    Render* render;
    QWidget tab_Widget_01;
    QWidget tab_Widget_02;
    QGridLayout tab_gridLayout_01;
    QGridLayout tab_gridLayout_02;
    QSplitter   tab_splitterLayout_01;

    QWidget tab_Widget_01_onTop;
    QWidget tab_Widget_01_onBottom;
    QVBoxLayout tab_Widget_01_layout;

    QTreeWidget tab_Widget_01_onTop_treeWidget;
    QVBoxLayout tab_Widget_01_onTop_layout;

    QWidget tab_Widget_01_onBottom_treeWidget;
    QVBoxLayout tab_Widget_01_onBottom_layout;

    QTreeWidgetItem* item;

    PropertyControl bottom_widget;
    QHBoxLayout hb_bottom_widget;
public:    
    QGridLayout* m_OpenGLlayout;
};
