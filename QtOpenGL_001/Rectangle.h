#pragma once
#include "BasePrimitives.h"

class Rectangle: public BasePrimitives
{
public:

	bool isEnable = false;
	//建立顶点数据
	float vertices[9] = {
	-0.5f, -0.5f, 0.0f,
	0.5f, -0.5f, 0.0f,
	0.0f, 0.5f, 0.0f
	};
	Rectangle() {

	};
	~Rectangle() {};
	virtual void initDraw(QVector<float> point);
	virtual void Draw();
	virtual void destory() {}
	virtual void scale(enum class C_Direction direction);
	virtual void move(enum class C_Direction direction);
	virtual void rotate(enum class C_Direction direction);
	virtual void setColor(COLOR_CUSTOM_RGBA color);
private:
	COLOR_CUSTOM_RGBA m_color;
};

