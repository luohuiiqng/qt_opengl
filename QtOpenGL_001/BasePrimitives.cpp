﻿#include "BasePrimitives.h"
#include <QDebug>
#include <iostream>
#include <QString>

BasePrimitives::BasePrimitives() {
	m_uid = QUuid::createUuid().toString().remove("{").remove("}");
	m_name = QString(m_uid.split('-')[0]);
}

void BasePrimitives::resetAll(QString uid) {

}

void BasePrimitives::setVertexShader(const char* vertexShaderSource) {
	m_structData.vertexShader = glCreateShader(GL_VERTEX_SHADER);
	int success;
	char infoLog[512];
	glShaderSource(m_structData.vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(m_structData.vertexShader);
	glGetShaderiv(m_structData.vertexShader, GL_COMPILE_STATUS, &success);	
	if (!success)
	{
		glGetShaderInfoLog(m_structData.vertexShader, 512, NULL, infoLog);
		qInfo() << QStringLiteral("顶点着色器编译失败") << QString::fromLocal8Bit(infoLog);
		return;
	}
}

void BasePrimitives::setFragmentShader(const char* fragmentShaderSource) {	
	int success;
	char infoLog[512];
	QString message;
	m_structData.fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(m_structData.fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(m_structData.fragmentShader);
	glGetShaderiv(m_structData.fragmentShader, GL_COMPILE_STATUS, &success);	
	if (!success)
	{
		glGetShaderInfoLog(m_structData.fragmentShader, 512, NULL, infoLog);
		QString str = infoLog;		
		qInfo() << QStringLiteral("片段着色器编译失败") << QString::fromLocal8Bit(infoLog);
		std::cout << infoLog<<std::endl;
		return;
	}
}

void BasePrimitives::setShaderProgram() {

	m_structData.shaderProgram = glCreateProgram();
	glAttachShader(m_structData.shaderProgram, m_structData.vertexShader);
	glAttachShader(m_structData.shaderProgram, m_structData.fragmentShader);
	glLinkProgram(m_structData.shaderProgram);
	int success = -2;
	char infoLog[512];
	//判断是否链接成功
	glGetProgramiv(m_structData.shaderProgram, GL_LINK_STATUS, &success);
	if (success == -1)
	{
		glGetProgramInfoLog(m_structData.shaderProgram, 512, NULL, infoLog);
		qDebug() << "ERROR::SHADERPROGRAM" << infoLog;
		std::cout << "34343434:" << infoLog << std::endl;
		return;
	}
	//glDeleteShader(m_structData.vertexShader);
	//glDeleteShader(m_structData.fragmentShader);
}
