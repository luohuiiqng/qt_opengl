#include "PropertyControl.h"


PropertyControl::PropertyControl(QWidget* parent) :QWidget(parent) {

	this->setLayout(&m_hblayout);
	m_hblayout.addWidget(&m_transform);
	m_hblayout.setMargin(1);
	m_hblayout.setSpacing(1);
	connect(&m_transform, &PropertyControlChild::sigPositionChanged, this, &PropertyControl::sigPosition);
	connect(&m_transform, &PropertyControlChild::sigRotateChanged, this, &PropertyControl::sigRotate);
	connect(&m_transform, &PropertyControlChild::sigScaleChanged, this, &PropertyControl::sigScale);//sigRsetAll
	connect(&m_transform, &PropertyControlChild::sigRsetAll, this, &PropertyControl::sigRsetAll);

}

PropertyControl::~PropertyControl() {

}


void PropertyControl::initCommon(commonData data) {

}
void PropertyControl::destoryed() {

}

void PropertyControl::init(MyCustomVector position, MyCustomVector rotation, MyCustomVector scale,QString uid) {
	m_transform.setPosition(position);
	m_transform.setRotation(rotation);
	m_transform.setScale(scale);
	m_transform.setUid(uid);
}


void PropertyControl::setUid(QString uid) {
	m_uid = uid;
}