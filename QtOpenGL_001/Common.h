#pragma once
#include "../thirdPart/GLM/glm.hpp"
#include "../thirdPart/GLM/gtc/matrix_transform.hpp"
#include "../thirdPart/GLM/gtc/type_ptr.hpp"

struct MyCustomVector
{
	float x = 0.0;
	float y = 0.0;
	float z = 0.0;

	void init() {
		x = 0.0;
		y = 0.0;
		z = 0.0;
	}
};

struct commonData{
		unsigned int VAO = -1;
		unsigned int VBO = -1;
		unsigned int EBO = -1;
		unsigned int vertexShader = -1;
		unsigned int fragmentShader = -1;
		unsigned int shaderProgram = -1;
		QString str_Fragment = "";//片元着色器
		QString str_Ver = "";//顶点着色器
		QVector<float> vertex;//顶点数据
		QString name = "";
		MyCustomVector rotate;
		MyCustomVector transform;
		MyCustomVector scale;
	};
