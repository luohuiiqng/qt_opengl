#pragma once
#include <qwidget.h>
#include "Common.h"
#include <QHBoxLayout>
#include "PropertyControlChild.h"

class PropertyControl :
    public QWidget
{
    Q_OBJECT
public:
    PropertyControl(QWidget* parent = nullptr);
    ~PropertyControl();
    void initCommon(commonData data);
    void init(MyCustomVector position, MyCustomVector rotation, MyCustomVector scale, QString uid);
    void setUid(QString uid);
public:
signals:
    void sigPosition(MyCustomVector vector,QString uid);
    void sigRotate(MyCustomVector vector, QString uid);
    void sigScale(MyCustomVector vector,QString uid);
    void sigRsetAll(QString uid);
private:
    void destoryed();
    QHBoxLayout m_hblayout;   
    PropertyControlChild m_transform;    
    QString m_uid="";
};

