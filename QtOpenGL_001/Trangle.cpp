﻿#include "Trangle.h"
#include <QDebug>
#include "Common.h"


const char* vertexShaderSource = "#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"
"uniform mat4 transform;\n"
"void main()\n"
"{\n"
" gl_Position = transform * vec4(aPos,1.0);\n"
"}\0";

const char* fragmentShaderSource = "#version 330 core\n"
"out vec4 FragColor;\n"
"uniform vec4 ourColor;\n"
"void main()\n"
"{\n"
" FragColor = ourColor;\n "
"}\0";



void Trangle::initDraw(QVector<float> point) {
	initializeOpenGLFunctions();//初始化
	isEnable = false;
	m_transform = glm::mat4(1.0f);
	if (point.size() < 3) {
		qInfo() << QStringLiteral("最小个数为三.");
		isEnable = false;
		return;
	}
	for (int i = 0; i < point.size(); i++) {
		switch (i)
		{
		case 0:
			vertices[0] = point[i];
		case 1:
			vertices[1] = point[i];
		case 2:
			vertices[3] = point[i];
		case 3:
			vertices[4] = point[i];
		case 4:
			vertices[6] = point[i];
		case 5:
			vertices[7] = point[i];	
		default:
			break;
		}
	}
	setVertexShader(vertexShaderSource);
	setFragmentShader(fragmentShaderSource);
	setShaderProgram();
	//配置顶点属性	
	glGenVertexArrays(1, &getVAO());
	glGenBuffers(1, &getVBO());

	//绑定VAO
	glBindVertexArray(getVAO());

	//绑定并设置VBO

	glBindBuffer(GL_ARRAY_BUFFER, getVBO());
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	//配置顶点属性
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	isEnable = true;
}


void Trangle::Draw() {
	//着色		
	glUseProgram(getShaderProgeam());
	int colorLocation = glGetUniformLocation(getShaderProgeam(), "ourColor");
	glUniform4f(colorLocation, m_color.r, m_color.g,m_color.b, m_color.a);	
	int transformLocation = glGetUniformLocation(getShaderProgeam(), "transform");
	
	glUniformMatrix4fv(transformLocation, 1, GL_FALSE, glm::value_ptr(m_transform));	
	glBindVertexArray(getVAO());
	glDrawArrays(GL_TRIANGLES, 0, 3);
}
void Trangle::setColor(COLOR_CUSTOM_RGBA color) {
	m_color = color;
}
void Trangle::scale(MyCustomVector vector) {	
	m_transform = glm::scale(m_transform, glm::vec3(vector.x, vector.y, vector.z));
	m_scale = vector;
}
void Trangle::move(MyCustomVector vector) {	
	m_transform = glm::translate(m_transform, glm::vec3(vector.x, vector.y, vector.z));
	m_position = vector;
}
void Trangle::rotate(MyCustomVector vector) {
	m_transform = glm::rotate(m_transform,(float)0.5, glm::vec3(vector.x, vector.y, vector.z));
	m_rotation = vector;
}

void Trangle::resetAll() {
	m_transform = glm::mat4(1.0f);
}

