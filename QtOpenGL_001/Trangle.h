﻿#pragma once

#include "BasePrimitives.h"
#include <QOpenGLFunctions_3_3_Core>
#include <QColor>

class Trangle: public BasePrimitives
{
public:
	
	bool isEnable = false;
	//建立顶点数据
	float vertices[9] = {
	-0.5f, -0.5f, 0.0f,
	0.5f, -0.5f, 0.0f,
	0.0f, 0.5f, 0.0f
	};
	Trangle() {		
	};
	~Trangle() {};
	virtual void initDraw(QVector<float> point);
	virtual void Draw();
	virtual void destory() {}
	virtual void scale(MyCustomVector vector);
	virtual void move(MyCustomVector vector);
	virtual void rotate(MyCustomVector vector);
	virtual void setColor(COLOR_CUSTOM_RGBA color);
	virtual void resetAll();
	MyCustomVector getScale() {
		return m_scale;
	}
	MyCustomVector getPosition() {
		return m_position;
	}
	MyCustomVector getRotation() {
		return m_rotation;
	}
private:
	COLOR_CUSTOM_RGBA m_color;
	MyCustomVector m_position;
	MyCustomVector m_scale;
	MyCustomVector m_rotation;
	//MyCustomVector m_transform;
};

