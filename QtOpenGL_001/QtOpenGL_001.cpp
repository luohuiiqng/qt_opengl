﻿#include "QtOpenGL_001.h"
#include <QPushButton>
#include <QColorDialog>
#include <QTreeWidgetItem>
#include <QListWidgetItem>
#include <QDebug>

QtOpenGL_001::QtOpenGL_001(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
    render = new Render(this);
    m_OpenGLlayout = new QGridLayout(ui.widget_4);
    m_OpenGLlayout->setMargin(0);
    m_OpenGLlayout->setSpacing(0);
    m_OpenGLlayout->addWidget(render);    
    connect(ui.btn_color,SIGNAL(clicked()), this, SLOT(slotColorDialog()));
    initFace();
    initTreeOne();
    initSignal();
}

QtOpenGL_001::~QtOpenGL_001()
{}

void QtOpenGL_001::initFace() {
    ui.tabWidget->setTabText(0, QStringLiteral("属性页"));
    ui.tabWidget->setTabText(1, QStringLiteral("Default"));
    tab_splitterLayout_01.setOrientation(Qt::Vertical);
    tab_splitterLayout_01.setContentsMargins(0,0,0,0);
    tab_gridLayout_01.addWidget(&tab_splitterLayout_01);
    ui.tabWidget->widget(0)->setLayout(&tab_gridLayout_01);
    ui.tabWidget->widget(1)->setLayout(&tab_gridLayout_02);
    tab_gridLayout_01.addWidget(&tab_Widget_01);
    tab_gridLayout_02.addWidget(&tab_Widget_02);
    tab_gridLayout_01.setMargin(1);
    tab_Widget_01.setStyleSheet(QString("background-color:#60746b"));

    tab_Widget_01.setLayout(&tab_Widget_01_layout);
    tab_splitterLayout_01.addWidget(&tab_Widget_01_onTop);
    tab_splitterLayout_01.addWidget(&tab_Widget_01_onBottom);
    tab_Widget_01_onTop.setStyleSheet(QString("background-color:#e4abb1"));
    tab_Widget_01_onBottom.setStyleSheet(QString("background-color:#281114"));

    tab_Widget_01_onTop.setLayout(&tab_Widget_01_onTop_layout);
    tab_Widget_01_onTop_layout.addWidget(&tab_Widget_01_onTop_treeWidget);
    tab_Widget_01_onTop_treeWidget.setStyleSheet(QString("background-color:#d3d5a1"));

    tab_Widget_01_onBottom.setLayout(&tab_Widget_01_onBottom_layout);
    tab_Widget_01_onBottom_layout.addWidget(&tab_Widget_01_onBottom_treeWidget);
    tab_Widget_01_onBottom_treeWidget.setStyleSheet(QString("background-color:#e6e7e0"));
  
    tab_Widget_01_onBottom_treeWidget.setLayout(&hb_bottom_widget);
    hb_bottom_widget.addWidget(&bottom_widget);
    //bottom_widget.setStyleSheet(QString("background-color:red"));
    //bottom_widget.setMinimumHeight(this->height() / 2 - 80);
}

void QtOpenGL_001::initTreeOne()
{
    tab_Widget_01_onTop_treeWidget.clear();    //QTreeWidget清空
    tab_Widget_01_onTop_treeWidget.setFrameStyle(QFrame::NoFrame);  //框架样式
    tab_Widget_01_onTop_treeWidget.setHeaderLabel("Word");  //设置头的标题    
    //tab_Widget_01_onTop_treeWidget.setColumnCount(100);    //tree widget展示的列数    
    item = new QTreeWidgetItem();//创建一个Item
    //item->setFlags(Qt::NoItemFlags);
    //item->setFlags(Qt::ItemIsEnabled);
    //item->setSizeHint(1,QSize(this->width(),20));
    tab_Widget_01_onTop_treeWidget.addTopLevelItem(item);    
}

void QtOpenGL_001::initSignal() {
    connect(render, &Render::sigAddObject, this, &QtOpenGL_001::slotAddSignal);

    connect(&bottom_widget, &PropertyControl::sigPosition, this, &QtOpenGL_001::slotPosition);
    connect(&bottom_widget, &PropertyControl::sigRotate, this, &QtOpenGL_001::slotRotation);
    connect(&bottom_widget, &PropertyControl::sigScale, this, &QtOpenGL_001::slotScale);
    connect(&bottom_widget, &PropertyControl::sigRsetAll, this, &QtOpenGL_001::slotRsetAll);
}

void QtOpenGL_001::slotPosition(MyCustomVector position, QString uid) {
    Trangle* tt1 = render->findObjByUid(uid);
    if (tt1 == NULL)return;
    tt1->move(position);
}
void QtOpenGL_001::slotRotation(MyCustomVector rotation, QString uid){
    Trangle* tt1 = render->findObjByUid(uid);
    if (tt1 == NULL)return;
    tt1->rotate(rotation);
}
void QtOpenGL_001::slotScale(MyCustomVector scale, QString uid){
    Trangle* tt1 = render->findObjByUid(uid);
    if (tt1 == NULL)return;
    tt1->scale(scale);
}

void QtOpenGL_001::slotRsetAll(QString uid) {
    Trangle* tt1 = render->findObjByUid(uid);
    if (tt1 == NULL)return;
    tt1->resetAll();
}

int sql = 0;
void QtOpenGL_001::slotAddSignal(QString uid, QString name) {
    WordWidgetCustom* custom = new WordWidgetCustom(name, uid, this->width(), 25);
    //tab_Widget_01_onTop_treeWidget.setItemWidget(item,0, custom);
    connect(custom, &WordWidgetCustom::sigClicked, this, [=](QString uid) {
        Trangle* tt1 = render->findObjByUid(uid);
        bottom_widget.init(tt1->getPosition(),tt1->getRotation(),tt1->getScale(),uid);
        qDebug() << "21312";
        });
    QTreeWidgetItem *tt = new QTreeWidgetItem(item);    
    //custom->setParent(&item);
    Trangle* tt1 = render->findObjByUid(uid);
    bottom_widget.init(tt1->getPosition(), tt1->getRotation(), tt1->getScale(), uid);
    tt->treeWidget()->setItemWidget(tt, 0, custom);    
    sql += 1;
}

void QtOpenGL_001::slotObjectChanged(QString uid) {
        
}

void QtOpenGL_001::keyPressEvent(QKeyEvent * ev)
{
    switch (ev->key()) {
    case Qt::Key_W:
    {
        slotColorDialog();
    }
    break;
    case Qt::Key_A:
    {           
    }
    break;
    case Qt::Key_S:
    {        
    }
    break;
    case Qt::Key_D:
    {        
    }
    break;
    }
}

void QtOpenGL_001::keyReleaseEvent(QKeyEvent* ev)
{
}

void QtOpenGL_001::slotColorDialog() {
    QColor color = QColorDialog::getRgba();
    //Render::command.insert(Render::drawObj[0].getUid(), color);
    int red = color.red();
    int green = color.green();
    int blue = color.blue();
    int alph = color.alpha();
    QRgb mRgb = qRgb(color.red(), color.green(), color.blue());
    QColor mColor = QColor(mRgb);
    if (render->Obj_list.size() == 0) return;
    QString uid = render->Obj_list.at(0)->getUid();
    Trangle* tt1 = render->findObjByUid(uid);
    BasePrimitives::COLOR_CUSTOM_RGBA colorValue;
    colorValue.r = red / 255;
    colorValue.g = green / 255;
    colorValue.b = blue / 255;
    colorValue.a = alph / 255;
    tt1->setColor(colorValue);

}
