#pragma once
#include <QWidget>
#include "Common.h"
#include "ui_Widget.h"
class PropertyControlChild: public QWidget
{
	Q_OBJECT
public:
	PropertyControlChild(QWidget *parent = nullptr,QString uid = "");
	~PropertyControlChild();	
	void setValue(MyCustomVector Position, MyCustomVector Rotation, MyCustomVector Scale);
	void setPosition(MyCustomVector Position);
	void setRotation(MyCustomVector Rotation);
	void setScale(MyCustomVector Scale);
	MyCustomVector getPosition();
	MyCustomVector getRotation();
	MyCustomVector getScale();
	void setUid(QString uid) {
		m_Uid = uid;
	}
	QString getUid() {
		return m_Uid;
	}
private:
	void init();
public:
signals:
	void sigPositionChanged(const MyCustomVector &value,QString uid);
	void sigRotateChanged(const MyCustomVector &value, QString uid);
	void sigScaleChanged(const MyCustomVector &value, QString uid);
	void sigRsetAll(QString uid);
private slots:
	void slotPositionAddX();
	void slotPositionSubtractX();
	void slotPositionAddY();
	void slotPositionSubtractY();
	void slotPositionAddZ();
	void slotPositionSubtractZ();
	void slotRotationAddX();
	void slotRotationSubtractX();
	void slotRotationAddY();
	void slotRotationSubtractY();
	void slotRotationAddZ();
	void slotRotationSubtractZ();
	void slotScaleAddX();
	void slotScaleSubtractX();
	void slotScaleAddY();
	void slotScaleSubtractY();
	void slotScaleAddZ();
	void slotScaleSubtractZ();
	void slotRsetAll();
private:	
	Ui::PropertyChildControl ui;
	MyCustomVector m_Position;
	MyCustomVector m_Rotation;
	MyCustomVector m_Scale;	
	QString m_Uid = "";
	float step = 0.1;
public:
	bool isIgnore = false;
};

