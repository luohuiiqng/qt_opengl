#include "PropertyControlChild.h"
#include <QPushButton>
#include <QLabel>
#include <QRegExpValidator>
#include <math.h>

PropertyControlChild::PropertyControlChild(QWidget* parent,QString uid):QWidget(parent) {
	ui.setupUi(this);
	m_Position.init();
	m_Rotation.init();
	m_Scale.init();
	m_Uid = uid;
	connect(ui.pushButton, &QPushButton::clicked, this, &PropertyControlChild::slotPositionAddX);
	connect(ui.pushButton_2, &QPushButton::clicked, this, &PropertyControlChild::slotPositionSubtractX);
	connect(ui.pushButton_3, &QPushButton::clicked, this, &PropertyControlChild::slotPositionAddY);	
	connect(ui.pushButton_4, &QPushButton::clicked, this, &PropertyControlChild::slotPositionSubtractY);
	connect(ui.pushButton_5, &QPushButton::clicked, this, &PropertyControlChild::slotPositionAddZ);
	connect(ui.pushButton_6, &QPushButton::clicked, this, &PropertyControlChild::slotPositionSubtractZ);

	connect(ui.pushButton_19, &QPushButton::clicked, this, &PropertyControlChild::slotRotationAddX);
	connect(ui.pushButton_20, &QPushButton::clicked, this, &PropertyControlChild::slotRotationSubtractX);
	connect(ui.pushButton_21, &QPushButton::clicked, this, &PropertyControlChild::slotRotationAddY);
	connect(ui.pushButton_22, &QPushButton::clicked, this, &PropertyControlChild::slotRotationSubtractY);
	connect(ui.pushButton_23, &QPushButton::clicked, this, &PropertyControlChild::slotRotationAddZ);
	connect(ui.pushButton_24, &QPushButton::clicked, this, &PropertyControlChild::slotRotationSubtractZ);

	connect(ui.pushButton_25, &QPushButton::clicked, this, &PropertyControlChild::slotScaleAddX);
	connect(ui.pushButton_26, &QPushButton::clicked, this, &PropertyControlChild::slotScaleSubtractX);
	connect(ui.pushButton_27, &QPushButton::clicked, this, &PropertyControlChild::slotScaleAddY);
	connect(ui.pushButton_28, &QPushButton::clicked, this, &PropertyControlChild::slotScaleSubtractY);
	connect(ui.pushButton_29, &QPushButton::clicked, this, &PropertyControlChild::slotScaleAddZ);
	connect(ui.pushButton_30, &QPushButton::clicked, this, &PropertyControlChild::slotScaleSubtractZ);

	connect(ui.pushButton_7, &QPushButton::clicked, this, &PropertyControlChild::slotRsetAll);

	init();
	connect(ui.lineEdit, &QLineEdit::textChanged, [=](QString value) {
		if (isIgnore) {
			isIgnore = false;
			return;
		}
		m_Position.x = value.toFloat();		
		sigPositionChanged(m_Position, m_Uid);
	});
	ui.lineEdit->setValidator(new QRegExpValidator(QRegExp("^-?(([0-9]{0,16}(\\.[0-9]{1,16})$)|([0-9]+$))")));
	connect(ui.lineEdit_2, &QLineEdit::textChanged, [=](QString value) {
		if (isIgnore) {
			isIgnore = false;
			return;
		}
		m_Position.y = value.toFloat();		
		sigPositionChanged(m_Position, m_Uid);
		});
	ui.lineEdit_2->setValidator(new QRegExpValidator(QRegExp("^-?(([0-9]{0,16}(\\.[0-9]{1,16})$)|([0-9]+$))")));
	connect(ui.lineEdit_3, &QLineEdit::textChanged, [=](QString value) {
		if (isIgnore) {
			isIgnore = false;
			return;
		}
		m_Position.z = value.toFloat();		
		sigPositionChanged(m_Position, m_Uid);
		});
	ui.lineEdit_3->setValidator(new QRegExpValidator(QRegExp("^-?(([0-9]{0,16}(\\.[0-9]{1,16})$)|([0-9]+$))")));
	connect(ui.lineEdit_16, &QLineEdit::textChanged, [=](QString value) {
		if (isIgnore) {
			isIgnore = false;
			return;
		}
		m_Rotation.x = value.toFloat();		
		sigRotateChanged(m_Rotation, m_Uid);
		});
	ui.lineEdit_16->setValidator(new QRegExpValidator(QRegExp("^-?(([0-9]{0,16}(\\.[0-9]{1,16})$)|([0-9]+$))")));
	connect(ui.lineEdit_17, &QLineEdit::textChanged, [=](QString value) {
		if (isIgnore) {
			isIgnore = false;
			return;
		}
		m_Rotation.y = value.toFloat();		
		sigRotateChanged(m_Rotation, m_Uid);
		});
	ui.lineEdit_17->setValidator(new QRegExpValidator(QRegExp("^-?(([0-9]{0,16}(\\.[0-9]{1,16})$)|([0-9]+$))")));
	connect(ui.lineEdit_18, &QLineEdit::textChanged, [=](QString value) {
		if (isIgnore) {
			isIgnore = false;
			return;
		}
		m_Rotation.z = value.toFloat();		
		sigRotateChanged(m_Rotation, m_Uid);
		});
	ui.lineEdit_18->setValidator(new QRegExpValidator(QRegExp("^-?(([0-9]{0,16}(\\.[0-9]{1,16})$)|([0-9]+$))")));
	connect(ui.lineEdit_19, &QLineEdit::textChanged, [=](QString value) {
		if (isIgnore) {
			isIgnore = false;
			return;
		}
		m_Scale.x = value.toFloat();		
		sigScaleChanged(m_Scale, m_Uid);
		});
	ui.lineEdit_19->setValidator(new QRegExpValidator(QRegExp("^-?(([0-9]{0,16}(\\.[0-9]{1,16})$)|([0-9]+$))")));
	connect(ui.lineEdit_20, &QLineEdit::textChanged, [=](QString value) {
		if (isIgnore) {
			isIgnore = false;
			return;
		}
		m_Scale.y = value.toFloat();		
		sigScaleChanged(m_Scale, m_Uid);
		});
	ui.lineEdit_20->setValidator(new QRegExpValidator(QRegExp("^-?(([0-9]{0,16}(\\.[0-9]{1,16})$)|([0-9]+$))")));
	connect(ui.lineEdit_21, &QLineEdit::textChanged, [=](QString value) {
		if (isIgnore) {
			isIgnore = false;
			return;
		}
		m_Scale.z = value.toFloat();		
		sigScaleChanged(m_Scale, m_Uid);
		});
	ui.lineEdit_21->setValidator(new QRegExpValidator(QRegExp("^-?(([0-9]{0,16}(\\.[0-9]{1,16})$)|([0-9]+$))")));
	
}

void PropertyControlChild::init() {
	ui.lineEdit->setText(QString::number(m_Position.x));
	ui.lineEdit_2->setText(QString::number(m_Position.y));
	ui.lineEdit_3->setText(QString::number(m_Position.z));

	ui.lineEdit_16->setText(QString::number(m_Rotation.x));
	ui.lineEdit_17->setText(QString::number(m_Rotation.y));
	ui.lineEdit_18->setText(QString::number(m_Rotation.z));

	ui.lineEdit_19->setText(QString::number(m_Scale.x));
	ui.lineEdit_20->setText(QString::number(m_Scale.y));
	ui.lineEdit_21->setText(QString::number(m_Scale.z));
}


PropertyControlChild::~PropertyControlChild() {

}

void PropertyControlChild::setValue(MyCustomVector Position, MyCustomVector Rotation, MyCustomVector Scale) {
	//m_Position = Position;
	//m_Rotation = Rotation;
	//m_Scale = Scale;
}
void PropertyControlChild::setPosition(MyCustomVector Position)
{
	m_Position = Position;
	ui.lineEdit->setText(QString::number(m_Position.x));
	ui.lineEdit_2->setText(QString::number(m_Position.y));
	ui.lineEdit_3->setText(QString::number(m_Position.z));	
}
void PropertyControlChild::setRotation(MyCustomVector rotation)
{
	m_Rotation = rotation;
	ui.lineEdit_16->setText(QString::number(m_Rotation.x));
	ui.lineEdit_17->setText(QString::number(m_Rotation.y));
	ui.lineEdit_18->setText(QString::number(m_Rotation.z));
}
void PropertyControlChild::setScale(MyCustomVector Scale)
{
	m_Scale = Scale;	
	ui.lineEdit_19->setText(QString::number(m_Scale.x));
	ui.lineEdit_20->setText(QString::number(m_Scale.y));
	ui.lineEdit_21->setText(QString::number(m_Scale.z));
}
MyCustomVector PropertyControlChild::getPosition()
{
	return m_Position;
}
MyCustomVector PropertyControlChild::getRotation()
{
	return m_Rotation;
}
MyCustomVector PropertyControlChild::getScale()
{
	return m_Scale;
}



void PropertyControlChild::slotPositionAddX() {
	isIgnore = true;
	m_Position.init();
	m_Position.x = step;	
	sigPositionChanged(m_Position, m_Uid);
	ui.lineEdit->setText(QString::number(m_Position.x));
	isIgnore = false;
}
void PropertyControlChild::slotPositionSubtractX() {
	isIgnore = true;
	m_Position.init();
	m_Position.x = -step;
	sigPositionChanged(m_Position, m_Uid);
	ui.lineEdit->setText(QString::number(m_Position.x));
	isIgnore = false;
}
void PropertyControlChild::slotPositionAddY() {
	isIgnore = true;
	m_Position.init();
	m_Position.y = step;
	//m_Position.y = round(m_Position.x * 10);
	sigPositionChanged(m_Position, m_Uid);
	ui.lineEdit_2->setText(QString::number(m_Position.y));	
}
void PropertyControlChild::slotPositionSubtractY() {
	isIgnore = true;
	m_Position.init();
	m_Position.y = -step;
	sigPositionChanged(m_Position, m_Uid);
	ui.lineEdit_2->setText(QString::number(m_Position.y));
	isIgnore = false;
}
void PropertyControlChild::slotPositionAddZ() {
	isIgnore = true;
	m_Position.init();
	m_Position.z = step;
	sigPositionChanged(m_Position, m_Uid);
	ui.lineEdit_3->setText(QString::number(m_Position.z));	
}
void PropertyControlChild::slotPositionSubtractZ() {
	isIgnore = true;
	m_Position.init();
	m_Position.z = -step;
	sigPositionChanged(m_Position, m_Uid);
	ui.lineEdit_3->setText(QString::number(m_Position.z));	
}
void PropertyControlChild::slotRotationAddX() {
	isIgnore = true;
	m_Rotation.init();
	m_Rotation.x = step;
	sigRotateChanged(m_Rotation, m_Uid);
	ui.lineEdit_16->setText(QString::number(m_Rotation.x));	
}
void PropertyControlChild::slotRotationSubtractX() {
	isIgnore = true;
	m_Rotation.init();
	m_Rotation.x = -step;
	sigRotateChanged(m_Rotation, m_Uid);
	ui.lineEdit_16->setText(QString::number(m_Rotation.x));	
}
void PropertyControlChild::slotRotationAddY() {
	isIgnore = true;
	m_Rotation.init();
	m_Rotation.y = step;
	sigRotateChanged(m_Rotation, m_Uid);
	ui.lineEdit_17->setText(QString::number(m_Rotation.y));	
}
void PropertyControlChild::slotRotationSubtractY() {
	isIgnore = true;
	m_Rotation.init();
	m_Rotation.y = -step;
	sigRotateChanged(m_Rotation, m_Uid);
	ui.lineEdit_17->setText(QString::number(m_Rotation.y));	
}
void PropertyControlChild::slotRotationAddZ() {
	isIgnore = true;
	m_Rotation.init();
	m_Rotation.z = step;
	sigRotateChanged(m_Rotation, m_Uid);
	ui.lineEdit_18->setText(QString::number(m_Rotation.z));	
}
void PropertyControlChild::slotRotationSubtractZ() {
	isIgnore = true;
	m_Rotation.init();
	m_Rotation.z = -step;
	sigRotateChanged(m_Rotation, m_Uid);
	ui.lineEdit_18->setText(QString::number(m_Rotation.z));	
}
void PropertyControlChild::slotScaleAddX() {
	isIgnore = true;
	m_Scale.init();
	m_Scale.x = 0.05;
	sigScaleChanged(m_Scale, m_Uid);
	ui.lineEdit_19->setText(QString::number(m_Scale.x));	
}
void PropertyControlChild::slotScaleSubtractX() {
	isIgnore = true;
	m_Scale.init();
	m_Scale.x = -0.05;
	sigScaleChanged(m_Scale, m_Uid);
	ui.lineEdit_19->setText(QString::number(m_Scale.x));	
}
void PropertyControlChild::slotScaleAddY() {
	isIgnore = true;
	m_Scale.init();
	m_Scale.y = 0.05;
	sigScaleChanged(m_Scale, m_Uid);
	ui.lineEdit_20->setText(QString::number(m_Scale.y));	
}
void PropertyControlChild::slotScaleSubtractY() {
	isIgnore = true;
	m_Scale.init();
	m_Scale.x = -0.05;
	sigScaleChanged(m_Scale, m_Uid);
	ui.lineEdit_20->setText(QString::number(m_Scale.y));	
}
void PropertyControlChild::slotScaleAddZ() {
	isIgnore = true;
	m_Scale.init();
	m_Scale.z = 0.05;
	sigScaleChanged(m_Scale, m_Uid);
	ui.lineEdit_21->setText(QString::number(m_Scale.z));	
}
void PropertyControlChild::slotScaleSubtractZ() {
	isIgnore = true;
	m_Scale.init();
	m_Scale.z = -0.05;
	sigScaleChanged(m_Scale, m_Uid);
	ui.lineEdit_21->setText(QString::number(m_Scale.z));	
}

void PropertyControlChild::slotRsetAll() {
	emit sigRsetAll(m_Uid);
}
